"""
# robin.py
## Eric Minaker
## Required modules
+ Tweepy
+ BeautifulSoup
+ NLTK

# Purpose
robin.py is built to parse through all tweets from the Michigan State
Legislature, counting the number of tweets for each user and classifying
the language of the tweet using the Naive Bayes algorithm. For each user it 
will report the number of tweets, quotes, retweets, and replies between the 
dates of [[DATE CUTOFF]] and [[DATE CUTOFF]]. It will also parse the language
of the tweets during that time period using NLTK and report the best fit
category for the tweet according from the six following:

    + Official Communications
      + Tweets discussing committee actions, current and pending legislation,
      or votes and positions taken.
    + Location and Activity
      + Tweets that update followers on the whereabouts of the legislator or
      inform them about upcoming or current fundraisers, meet-and-greets, and
      other events.
    + Current Events
      + Discussion of current news events, including presidential actions.
    + Local Events
      + News stories and updates about local going ons
    + Unofficial Communications
      + Non-political discussion that include posts about sporting events,
      holidays, or other special occurrences.

This report will be saved to a file called "robin.results" in the directory
that the script was executed in. It will contain the list of screens names
and the percentage of tweets for each category along with the total number
of tweets for that user, broken down by the type of tweet into the following
categories:
     
    + Quoted tweets
    + Retweets
    + Replies to tweets
    + Normal tweets

Furthermore, the detailed results for each given screen name will be saved as 
"screen_name.results" in the directory that the script was executed in. These
files will identify the user and then proceed to list each tweet alongisde
the percentage match to each category, which category was chosen, and whether
or not the tweet is a retweet, quoted tweet, or reply. It will also give the
number of each type of tweet for the user.
 
# Operation
In order to run, the script requires an input file given in the command line
as the only argument. The file must be an HTML document with hyperlinks to
each of the Twitter accounts that should be analyzed. The file cannot contain
any hyperlinks other than those of the Twitter accounts to be analyzed. The
script will parse the screen names from the hyperlinks.

The total run time in dependent on the number of accounts being analyzed and
how many tweets each account has. As it honors the Twitter API rate limits
and does not use multiple APIs to circumvent them, it can take quite a while.
Because of this it is recommended to run the script on a persistently online
server and have it email you a notification when it finishes. It is
recommended that the script be placed in its own directory with write
permissions.

**Be aware that robin.py will generate many files, some rather large. Ensure
that the server has the capacity to deal with this.**
 
# How it Works
The script will, using BeautifulSoup, find each link in the given HTML file
and store them all in an array. For each link in that array, it will split
them on any slash characters ('/') and place the last element of the
resulting array into a new array of screen names.

It will loop through the array of users and, using Tweepy, request all tweets
of a given Twitter account. The tweets will be stored into an array and
iterated over to fetch the type of each tweet, the types will then be counted
saved to a file.

The array of tweets will be iterated over a second time in order to parse
each one with NLTK. Each tweet will be passed through the Naive Bayes
classification algorithm using previously hand-coded sets of tweets to train
classifiers for the six categories. The category with the highest percentage
match to the tweet will be selected for the tweet and saved to an array. The
tweet and percentage match to each category will be written to a file. After
all tweets for a screen name have been parsed, the percentage of tweets in
each category will be calculated and written to file.
"""
import tweepy
from tweepy import OAuthHandler
from datetime import datetime
import time
from time import mktime
from BeautifulSoup import BeautifulSoup
import urllib3
import re
import sys
import nltk
import collections
import nltk.classify.util
from nltk.classify import NaiveBayesClassifier
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import movie_reviews
from nltk.corpus import stopwords


import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

urllib3.disable_warnings()

def initAPI():
    # Initialize basic API settings
    consumer_key = '6G6M1osYAWd3YpmCrSq9EGwxy'
    consumer_secret = 'qRjchFMackGV7bZFwByfjogIihEXTjtpEYZcdVXGO0UMHPJWck'
    access_token = '1650893270-AU95H87JMqWUQFiEKb7SnvBWzQy5t4ll4iq4imf'
    access_secret = 'wkhB657dLD4ZlzKBEOxvSeT9oaPbtYAIII6XhC5sr2N1d'
      
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_secret)
       
    api = tweepy.API(auth, wait_on_rate_limit=True)
    return api

def importAccounts():
    # Import account names from given HTML file
    soup = BeautifulSoup(open(str(sys.argv[1])))
    links = []
    reps = []
    for link in soup.findAll('a', attrs={'href': re.compile("^https://")}):
        links.append(link.get('href'))
    for link in links:
        reps.append(link.rsplit('/', 1)[-1])
    return reps;

def fetchTweetsFromUser(rep, api):
    tweets = []
    # Error handling catches accounts not found or locked as well as rate limit
    # violations and connections issues.
    #     Try:
    #         Get and process Tweet
    #     Except:
    #         Print error to file
    try:
        for status in tweepy.Cursor(api.user_timeline, id = rep).items():
            # Check if tweet occured after selected cutoff, count it
            tweetDate = str(status.created_at)
            maxDate = "2016-06-24 23:59:59"
            tweetDateProc = time.strptime(tweetDate, "%Y-%m-%d %H:%M:%S")
            maxDateProc = time.strptime(maxDate, "%Y-%m-%d %H:%M:%S")
            if(tweetDateProc > maxDateProc):
                tweets.append(status)

    # If the tweet cannot be accessed
    except Exception, e:
        print("    Failed to access Twitter account for " + rep +
                " Exception: " + str(e)+"\n")
        raise Exception
    return tweets

def fetchTweetById(tweetId, api):
    try:
        tweet = api.get_status(tweetId)
    except Exception, e:
        print 'Error: ' + e + ' fetching tweet ' + tweetId
        raise Exception
    return tweet

def countTweets(tweets): 
    normalTot = 0
    quoteTot = 0
    replyTot = 0
    retweetTot = 0
    tweetTot = 0
    for status in tweets:
        tweetType = getType(status)
        #print(tweetType + "\n")
        if(tweetType == 'quote'):
            quoteTot += 1
        if(tweetType == 'retweet'):
            retweetTot += 1
        if(tweetType == 'reply'):
            replyTot += 1
        if(tweetType == 'normal'):
            normalTot += 1
    tweetTot = quoteTot + retweetTot + replyTot + normalTot
    total = collections.namedtuple('Totals', 
               'quoteTotal replyTotal retweetTotal normalTotal tweetTotal')
    RT = total(quoteTotal = quoteTot, 
               replyTotal = replyTot, 
               retweetTotal = retweetTot, 
               normalTotal = normalTot, 
               tweetTotal = tweetTot)
    return RT

def printCount(rep, retweetTot, quoteTot, replyTot, normalTot, tweetTot):
    # Open file for writing
    f = open('repResults.test', 'a')
    f.write(rep+"\n")
    # If there's no tweets
    if(tweetTot == 0):
        f.write("    " + rep + " had no tweets since June 24th, 2016.\n")
        pass
    # All of the other file writing
    else:
        f.write("    Since June 24th, 2016 " + rep + " had:\n")
        f.write("        " + str(retweetTot) + " retweets\n")
        f.write("        " + str(quoteTot) + " quoted tweets\n")
        f.write("        " + str(replyTot) + " reply tweets\n")
        f.write("        " + str(normalTot) + " normalt tweets\n")
        f.write("        " + str(tweetTot) + " total tweets\n\n")
        print("Successful print for " + rep + "\n")
    f.close()

def getType(status):
    # Error handling in place as status.quoted_status can and often
    # will return nothing, breaking the program.
    #     Try:
    #         Count quoted tweet
    #     Except:
    #         Not a quoted tweet, pass
    tweetType = 'none'
    try:
        if(status.quoted_status):
            tweetType = 'quote'
    except:
        pass
    try:
        if(status.retweeted_status):
            tweetType = 'retweet'
    except:
        pass
    if(status.in_reply_to_screen_name):
        # If reply instead, count approriately
        tweetType = 'reply'
    elif(tweetType == 'none'):
        # If not, add total. Normal and quoted tweets will be
        # handled here
        tweetType = 'normal'
    return tweetType

def genFeats(tweets):
    feats = [(tweetFeats(f))]

def classifyTweets(officialTweets, locationTweets, currTweets, localTweets, unofficTweets):
    #tokens = nltk.word_tokenize(text)
    #print tokens
    ##negids = movie_reviews.fileids('neg')
    ##posids = movie_reviews.fileids('pos')
    # officialClass would need to be an array of status/tweet objects from 
    # Tweepy.
    #####officialClass = tweets
    #####locationClass = localTweets
    #####currentClass = currTweets
    # Builds the feature array for a given category by looping through each
    # hand-coded example of the training set and pulling out features.
    # For f in officialClass:
    #     officialFeats = word_feats(officialClass[f].text
    #for tweet in officialClass:
    #    officialFeats=tweetFeats(tweet)
    #for p in officialFeats:
        #print p
    officialFeats = [(tweetFeats(f), 'official') for f in officialTweets]
    locationFeats = [(tweetFeats(f), 'location') for f in locationTweets]
    currentFeats = [(tweetFeats(f), 'current') for f in currTweets]
    localFeats = [(tweetFeats(f), 'local') for f in localTweets]
    unofficFeats = [(tweetFeats(f), 'unoffic') for f in unofficTweets]
    ##posfeats = [(word_feats(movie_reviews.words(fileids=[f])), 'pos') for f in posids]
    ##negfeats = [(word_feats(movie_reviews.words(fileids=[f])), 'neg') for f in negids]
    ###for p in posfeats:
    ###    print p

    ##negcutoff = len(negfeats)*3/4
    ##poscutoff = len(posfeats)*3/4
    officialCutoff = len(officialFeats)*3/4
    locationCutoff = len(locationFeats)*3/4
    currentCutoff = len(currentFeats)*3/4
    localCutoff = len(localFeats)*3/4
    unofficCutoff = len(unofficFeats)*3/4
    ##trainfeats = negfeats[:negcutoff] + posfeats[:poscutoff]
    ##trainfeats = officialFeats[:officialCutoff] + posfeats[:poscutoff]
    trainFeats = officialFeats[:officialCutoff] + locationFeats[:locationCutoff] + currentFeats[:currentCutoff] + localFeats[:localCutoff] + unofficFeats[:unofficCutoff]
    ###trainFeats = officialFeats[:officialCutoff]
    testFeats = officialFeats[officialCutoff:] + locationFeats[locationCutoff:] + currentFeats[currentCutoff:] + localFeats[localCutoff:] + unofficFeats[unofficCutoff:]
    ###testFeats = officialFeats[officialCutoff:]
    ###print 'train on %d instances, test of %d instances' % (len(trainFeats), len(testFeats))
    ##testfeats = negfeats[negcutoff:] + posfeats[poscutoff:]
    ##testfeats = officialFeats[officialCutoff:] + posfeats[poscutoff:]
    print 'train on %d instances, test on %d instances' % (len(trainFeats), len(testFeats))

    ##classifier = NaiveBayesClassifier.train(trainfeats)
    classifier = NaiveBayesClassifier.train(trainFeats)
    print 'naive bayes features'
    i = 0
    for feat in trainFeats:
        for w in feat:
            for g in w:
                i += 1
    print i
    ###print 'accuracy: ', nltk.classify.util.accuracy(classifier, testFeats)
    
    print 'accuracy:', nltk.classify.util.accuracy(classifier, testFeats)
    classifier.show_most_informative_features()
    print 'labels: '
    print classifier.labels()
    return classifier

    

def word_feats(words):
    return dict([(word, True) for word in words])

def tweetFeats(tweet):
    stops = set(stopwords.words('english'))
    #for w in line.split():
    #    if w.lower() not in stops:
    #        tweetText = tweet.text.split()
    #tokenizer = RegexpTokenizer(r'\w+')
    #tokens = tokenizer.tokenize(tweetText)
    
    wordRet = {}
    for word in tweet.text.split():
        if word.lower() not in stops:
            if word.lower() != '&amp;' and word.lower() != '&nbsp;' and word.lower() != '|' and word.lower() != 'via':
                wordRet[word] = True
    #return dict([(word, True) for word in tweet.text.split()])
    return wordRet

def populateTrain(inf):
    trainedArray = []
    f = open(inf, "r")
    for l in f.readlines():
        trainedArray.append(l.rsplit('/',1)[-1])
    return trainedArray

def buildTweetArray(array, api):
    tweetArray = []
    for tId in array:
        tweetArray.append(fetchTweetById(tId, api))
    return tweetArray

def main():
    fe = open('tweettotals.out','a')

    officialCommTrain = populateTrain("officialCommunicationTestSet")
    locationActivityTrain = populateTrain("locationActivityTestSet")
    currentEventTrain = populateTrain("currentEventsTestSet")
    localEventTrain = populateTrain("localEventsTestSet")
    unofficialCommTrain = populateTrain("unofficialCommunicationsTestSet")
    error = False
    api = initAPI()
    officSet = []
    locatSet = []
    currSet = []
    localSet = []
    unofficSet = []

    officSecondarySet = []
    locatSecondarySet = []
    currSecondarySet = []
    localSecondarySet = []
    unofficSecondarySet = []

    officSet = buildTweetArray(officialCommTrain, api)
    locatSet = buildTweetArray(locationActivityTrain, api)
    currSet = buildTweetArray(currentEventTrain, api)
    localSet = buildTweetArray(localEventTrain, api)
    unofficSet = buildTweetArray(unofficialCommTrain, api)
    classifier = classifyTweets(officSet,locatSet,currSet,localSet,unofficSet)
    #print(tweetSet[0].text)

    reps = importAccounts()
    officCount = 0
    locatCount = 0
    currCount = 0
    localCount = 0
    unofficCount = 0
    for rep in reps:
        error = False
        try:
            print("Fetching tweets for " + rep + "\n")
            tweets = fetchTweetsFromUser(rep, api)
        except Exception, e:
            error = True
            pass
        if(error == False):
            print("Counting tweets for " + rep + "\n")
            totals = countTweets(tweets)
            for tweet in tweets:
                if "facebook" not in tweet.text.lower():
                    feats = tweetFeats(tweet)
                    print(tweet.text.encode('utf-8'))
                    tweetClass = classifier.classify(feats)
                    probs = classifier.prob_classify(feats)
                    if(probs.prob("official") >= 0.9 and tweet.id_str not in officSet):
                        officSecondarySet.append(tweet.id_str)
                    if(probs.prob("current") >= 0.9 and tweet.id_str not in currSet):
                        currSecondarySet.append(tweet.id_str)
                    if(probs.prob("location") >= 0.9 and tweet.id_str not in locatSet):
                        locatSecondarySet.append(tweet.id_str)
                    if(probs.prob("local") >= 0.9 and tweet.id_str not in localSet):
                        localSecondarySet.append(tweet.id_str)
                    if(probs.prob("unoffic") >= 0.9 and tweet.id_str not in unofficSet):
                        unofficSecondarySet.append(tweet.id_str)

                    print(tweetClass)
                    if(tweetClass=='official'):
                        officCount = officCount + 1
                    if(tweetClass=='location'):
                        locatCount = locatCount + 1
                    if(tweetClass=='current'):
                        currCount = currCount + 1
                    if(tweetClass=='unoffic'):
                        unofficCount = unofficCount + 1
                    if(tweetClass=='local'):
                        localCount = localCount + 1
                    #if guess != tag:
                    #    errors.append ( (tag, guess, cat) )
                    #for(tag, guess, cat) in sorted(errors):
                    #    fe.write('correct={:<8} guess={:<8s} cat={:<30}'.format(tag, guess, cat))
            printCount(rep, totals.quoteTotal, totals.replyTotal, 
                    totals.retweetTotal, totals.normalTotal, totals.tweetTotal)
    totalTweets= officCount + locatCount + currCount + unofficCount
    op = officCount/totalTweets*100.00
    lp = locatCount/totalTweets*100.00
    up = unofficCount/totalTweets*100.00
    cp = currCount/totalTweets*100.00
    lop = localCount/totalTweets*100.00
    fe.write("First Pass\n")
    fe.write("Total Tweest : " + str(totalTweets) + "\n")
    fe.write("Official Tweets: " + str(officCount) + " or " + str(op) + "% of total\n")
    fe.write("Location and Activity Tweets: " + str(locatCount) + " or " + str(lp) + "% of total\n")
    fe.write("Current Events Tweets: " + str(currCount) + " or " + str(cp)+ "% of total\n")
    fe.write("Unofficial Tweets: " + str(unofficCount) + " or " + str(up) + "% of total\n")
    fe.write("Local Tweets: " + str(localCount) + " or " + str(lop) + "% of total\n")
    fe.write("\n")
    fe.write("Length of Official Seconadry: " + str(len(officSecondarySet)) + "\n")
    fe.write("Length of Unofficial Secondary: " + str(len(unofficSecondarySet)) + "\n")
    fe.write("Length of Location Secondary: " + str(len(locatSecondarySet)) + "\n")
    fe.write("Length of Local Secondary: " + str(len(localSecondarySet)) + "\n")
    fe.write("Length of Current Secondary: " + str(len(currSecondarySet)) + "\n")
   
    officSecondarySet = buildTweetArray(officSecondarySet,api)
    unofficSecondarySet = buildTweetArray(unofficSecondarySet,api)
    locatSecondarySet = buildTweetArray(locatSecondarySet,api)
    localSecondarySet = buildTweetArray(localSecondarySet,api)
    currSecondarySet = buildTweetArray(currSecondarySet,api)

    newOfficSet = officSecondarySet + officSet
    newUnofficSet = unofficSecondarySet + unofficSet
    newLocatSet = locatSecondarySet + locatSet
    newLocalSet = localSecondarySet + localSet
    newCurrSet = currSecondarySet + currSet
    
    newClassifier = classifyTweets(newOfficSet,newLocatSet,newCurrSet,newLocalSet,newUnofficSet)
    
    newOfficSecondarySet = []
    newCurrSecondarySet = []
    newLocatSecondarySet = []
    newLocalSecondarySet = []
    newUnofficSecondarySet = []
    
    officCount = 0
    locatCount = 0
    localCount = 0
    currCount = 0
    unofficCount = 0

    for rep in reps:
        error = False
        try:
            print("Fetching tweets for " + rep + " for the second time\n")
            tweets = fetchTweetsFromUser(rep, api)
        except Exception, e:
            error = True
            pass
        if(error == False):
            for tweet in tweets:
                if "facebook" not in tweet.text.lower():
                    feats = tweetFeats(tweet)
                    tweetClass = newClassifier.classify(feats)
                    probs = newClassifier.prob_classify(feats)
                    if(probs.prob("official") >= 0.9 and tweet.id_str not in newOfficSet):
                        newOfficSecondarySet.append(tweet.id_str)
                    if(probs.prob("current") >= 0.9 and tweet.id_str not in newCurrSet):
                        newCurrSecondarySet.append(tweet.id_str)
                    if(probs.prob("location") >= 0.9 and tweet.id_str not in newLocatSet):
                        newLocatSecondarySet.append(tweet.id_str)
                    if(probs.prob("local") >= 0.9 and tweet.id_str not in newLocalSet):
                        newLocalSecondarySet.append(tweet.id_str)
                    if(probs.prob("unoffic") >= 0.9 and tweet.id_str not in newUnofficSet):
                        newUnofficSecondarySet.append(tweet.id_str)

                    print(tweetClass)
                    if(tweetClass=='official'):
                        officCount = officCount + 1
                    if(tweetClass=='location'):
                        locatCount = locatCount + 1
                    if(tweetClass=='current'):
                        currCount = currCount + 1
                    if(tweetClass=='unoffic'):
                        unofficCount = unofficCount + 1
                    if(tweetClass=='local'):
                        localCount = localCount + 1
                    #if guess != tag:
                    #    errors.append ( (tag, guess, cat) )
                    #for(tag, guess, cat) in sorted(errors):
                    #    fe.write('correct={:<8} guess={:<8s} cat={:<30}'.format(tag, guess, cat))
        
    totalTweets= officCount + locatCount + currCount + unofficCount
    op = officCount/totalTweets*100.00
    lp = locatCount/totalTweets*100.00
    up = unofficCount/totalTweets*100.00
    cp = currCount/totalTweets*100.00
    lop = localCount/totalTweets*100.00
    fe.write("Second Pass\n")
    fe.write("Official Tweets: " + str(officCount) + " or " + str(op) + "% of total\n")
    fe.write("Location and Activity Tweets: " + str(locatCount) + " or " + str(lp) + "% of total\n")
    fe.write("Current Events Tweets: " + str(currCount) + " or " + str(cp)+ "% of total\n")
    fe.write("Unofficial Tweets: " + str(unofficCount) + " or " + str(up) + "% of total\n")
    fe.write("Local Tweets: " + str(localCount) + " or " + str(lop) + "% of total\n")
    fe.write("\n")
    fe.write("Length of Official Seconadry: " + str(len(newOfficSecondarySet)) + "\n")
    fe.write("Length of Unofficial Secondary: " + str(len(newUnofficSecondarySet)) + "\n")
    fe.write("Length of Location Secondary: " + str(len(newLocatSecondarySet)) + "\n")
    fe.write("Length of Local Secondary: " + str(len(newLocalSecondarySet)) + "\n")
    fe.write("Length of Current Secondary: " + str(len(newCurrSecondarySet)) + "\n")
    
    newOfficSecondarySet = buildTweetArray(newOfficSecondarySet,api)
    newUnofficSecondarySet = buildTweetArray(newUnofficSecondarySet,api)
    newLocatSecondarySet = buildTweetArray(newLocatSecondarySet,api)
    newLocalSecondarySet = buildTweetArray(newLocalSecondarySet,api)
    newCurrSecondarySet = buildTweetArray(newCurrSecondarySet,api)

    newNewOfficSet = newOfficSecondarySet + newOfficSet
    newNewUnofficSet = newUnofficSecondarySet + newUnofficSet
    newNewLocatSet = newLocatSecondarySet + newLocatSet
    newNewLocalSet = newLocalSecondarySet + newLocalSet
    newNewCurrSet = newCurrSecondarySet + newCurrSet
    
    newNewClassifier = classifyTweets(newNewOfficSet,newNewLocatSet,newNewCurrSet,newNewLocalSet,newNewUnofficSet)
    
    newOfficTertSet = []
    newCurrTertSet = []
    newLocatTertSet = []
    newLocalTertSet = []
    newUnofficTertSet = []
    
    officCount = 0
    locatCount = 0
    localCount = 0
    currCount = 0
    unofficCount = 0

    for rep in reps:
        error = False
        try:
            print("Fetching tweets for " + rep + " for the second time\n")
            tweets = fetchTweetsFromUser(rep, api)
        except Exception, e:
            error = True
            pass
        if(error == False):
            for tweet in tweets:
                if "facebook" not in tweet.text.lower():
                    feats = tweetFeats(tweet)
                    tweetClass = newNewClassifier.classify(feats)
                    probs = newNewClassifier.prob_classify(feats)
                    if(probs.prob("official") >= 0.9 and tweet.id_str not in newNewOfficSet):
                        newOfficTertSet.append(tweet.id_str)
                    if(probs.prob("current") >= 0.9 and tweet.id_str not in newNewCurrSet):
                        newCurrTertSet.append(tweet.id_str)
                    if(probs.prob("location") >= 0.9 and tweet.id_str not in newNewLocatSet):
                        newLocatTertSet.append(tweet.id_str)
                    if(probs.prob("local") >= 0.9 and tweet.id_str not in newNewLocalSet):
                        newLocalTertSet.append(tweet.id_str)
                    if(probs.prob("unoffic") >= 0.9 and tweet.id_str not in newNewUnofficSet):
                        newUnofficTertSet.append(tweet.id_str)

                    print(tweetClass)
                    if(tweetClass=='official'):
                        officCount = officCount + 1
                    if(tweetClass=='location'):
                        locatCount = locatCount + 1
                    if(tweetClass=='current'):
                        currCount = currCount + 1
                    if(tweetClass=='unoffic'):
                        unofficCount = unofficCount + 1
                    if(tweetClass=='local'):
                        localCount = localCount + 1
                    #if guess != tag:
                    #    errors.append ( (tag, guess, cat) )
                    #for(tag, guess, cat) in sorted(errors):
                    #    fe.write('correct={:<8} guess={:<8s} cat={:<30}'.format(tag, guess, cat))
        
    totalTweets= officCount + locatCount + currCount + unofficCount
    op = officCount/totalTweets*100.00
    lp = locatCount/totalTweets*100.00
    up = unofficCount/totalTweets*100.00
    cp = currCount/totalTweets*100.00
    lop = localCount/totalTweets*100.00
    fe.write("Second Pass\n")
    fe.write("Official Tweets: " + str(officCount) + " or " + str(op) + "% of total\n")
    fe.write("Location and Activity Tweets: " + str(locatCount) + " or " + str(lp) + "% of total\n")
    fe.write("Current Events Tweets: " + str(currCount) + " or " + str(cp)+ "% of total\n")
    fe.write("Unofficial Tweets: " + str(unofficCount) + " or " + str(up) + "% of total\n")
    fe.write("Local Tweets: " + str(localCount) + " or " + str(lop) + "% of total\n")
    fe.write("\n")
    fe.write("Length of Official Seconadry: " + str(len(newOfficTertSet)) + "\n")
    fe.write("Length of Unofficial Secondary: " + str(len(newUnofficTertSet)) + "\n")
    fe.write("Length of Location Secondary: " + str(len(newLocatTertSet)) + "\n")
    fe.write("Length of Local Secondary: " + str(len(newLocalTertSet)) + "\n")
    fe.write("Length of Current Secondary: " + str(len(newCurrTertSet)) + "\n")
    
    
    fe.close()

main()
